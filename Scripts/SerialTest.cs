﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SerialTest : MonoBehaviour {

    void Update()
    {
        if(SerialPortManagr.Instance.InputKeyClickDown(KeyType.圆形按钮))
        {
            print("点击了圆形按钮");
        }

        if (SerialPortManagr.Instance.anyKeyDown())
        {
            foreach (KeyType type in Enum.GetValues(typeof(KeyType)))
            {
                if (SerialPortManagr.Instance.InputKeyClickDown(type))
                {
                    print("点击了" + type);
                }
            }
        }
    }
}
